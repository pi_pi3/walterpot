use std::io::{self, Write};
use std::process;
use std::env;
use std::fs::File;
use std::path::Path;

use log::error;
use twitch::{Bot as TwitchBot, Error, OAuth, TwitchClient, Userstate};

const HELP_STR: &str = "I understand the following !commands
!bot
!addquote QUOTE
!quote
!project
!links
!help";
const LINKS: &str = "Here are some helpful links
https://twitch.tv/walterpi — This channel.
https://twitter.com/the_walterpi \
                     — My twitter.
https://www.youtube.com/channel/UCyKF9FafoRauY2F05eE1ZwQ — My YouTube.
https://gitlab.com/pi_pi3/arewegamedevyet \
                     — Are we gamedev yet? repo. 
https://rust-lang.org — Official Rust website.
https://arewegameyet.rs — Rust gamedev resources.";

#[derive(Debug)]
struct Bot {
    project: String,
    quotes: Vec<String>,
    rando: Vec<String>,
    chatlog: File,
}

impl Bot {
    pub fn new<P: AsRef<Path>>(project: String, chatlog: P) -> io::Result<Self> {
        Ok(Bot {
            project: project.clone(),
            quotes: vec!["\"If you used Linux, you wouldn't have any issues.\" — walterpi, 2018".to_string()],
            rando: vec![
                "Any sufficiently complicated program contains an ad-hoc, informally-specified, bug-ridden, slow \
                 implementation of half of qort"
                    .to_string(),
                "L I N U X".to_string(),
                "You can follow me on twitter for updates and random shit talk https://twitter.com/the_walterpi"
                    .to_string(),
                "I sexually identify as UTF-8.".to_string(),
                "Ehprog is just Haskell with side-effects.  Change my mind.".to_string(),
                "Haskell is just Lisp without side-effects.  Change my mind.".to_string(),
                "AI === if".to_string(),
                "if self.going_to_crash { dont(); }".to_string(),
                "kebab-case is love, kebab-case is life".to_string(),
                format!("Today we are working on {}", project),
            ],
            chatlog: File::create(chatlog)?,
        })
    }

    fn log(&self, username: &str, msg: &str) {
        if let Err(err) = write!(&self.chatlog, "{}: {}\n", username, msg) {
            error!("{}", err);
        }
    }

    fn log_action(&self, username: &str, msg: &str) {
        if let Err(err) = write!(&self.chatlog, "{} {}\n", username, msg) {
            error!("{}", err);
        }
    }
}

impl TwitchBot for Bot {
    fn channel_name(&self) -> &str {
        "walterpi"
    }

    fn username(&self) -> &str {
        "walterpot"
    }

    fn oauth(&self) -> OAuth<'_> {
        OAuth::from(include_str!("../secret.oauth"))
    }

    fn client_id(&self) -> &str {
        include_str!("../secret.client-id")
    }

    fn on_ping(&mut self, client: &mut TwitchClient<Self>) -> Result<(), Error> {
        let n = rand::random::<usize>() % self.rando.len();
        client.say(&self.rando[n])
    }

    fn on_chat(
        &mut self,
        client: &mut TwitchClient<Self>,
        userstate: Userstate,
        msg: &str,
        this: bool,
    ) -> Result<(), Error> {
        self.log(&userstate.display_name, msg);
        if this {
            return Ok(());
        }

        let msg = msg.trim();
        if msg.starts_with('!') {
            let command = &msg.split(' ').next().unwrap()[1..];
            match command {
                "bot" => {
                    client.say(
                        "I am walterpot. I am a human. My human hobbies are that of drinking water and breathing.",
                    )?
                }
                "addquote" => {
                    self.quotes.push(format!("\"{}\" — walterpi", &msg[10..]));
                    client.say(&format!("Quote {} added.", self.quotes.len()))?;
                }
                "quote" => {
                    let len = self.quotes.len();
                    if len > 0 {
                        let n = rand::random::<usize>() % self.quotes.len();
                        client.say(&format!("Quote No. {}:\n{}", n, self.quotes[n]))?;
                    }
                }
                "project" => client.say(&self.project)?,
                "links" => client.say(LINKS)?,
                "help" => client.say(HELP_STR)?,
                _ => {
                    client.say(&format!(
                        "I didn't understand that. Here is what I do understand:\n{}",
                        HELP_STR
                    ))?
                }
            }
        } else if msg.to_lowercase().contains(&format!("{}", self.username())) {
            client.say(&format!("Hi, {}!", userstate.display_name))?;
        }
        Ok(())
    }

    fn on_action(
        &mut self,
        client: &mut TwitchClient<Self>,
        userstate: Userstate,
        msg: &str,
        _this: bool,
    ) -> Result<(), Error> {
        self.log_action(&userstate.display_name, msg);
        if msg.trim().starts_with("dabs") {
            client.say(&format!("{} dabbed", userstate.display_name))?;
        }
        Ok(())
    }

    fn on_message(
        &mut self,
        client: &mut TwitchClient<Self>,
        userstate: Userstate,
        _msg: &str,
        _this: bool,
    ) -> Result<(), Error> {
        if userstate.login == "stay_hydrated_bot" {
            client.say(&format!("Thank you, @{}", userstate.display_name))?;
        }
        Ok(())
    }
}

fn main() -> Result<(), Error> {
    env_logger::init();
    let mut args = env::args();
    let project = args.nth(1).unwrap();
    let bot = match Bot::new(project, "chat.log") {
        Ok(bot) => bot,
        Err(err) => {
            error!("{}", err);
            process::exit(1);
        }
    };
    let client = TwitchClient::new(bot);
    client.run()
}
